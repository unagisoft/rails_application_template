require 'rails_helper'

describe UsersController, type: :controller do
  let!(:user) { create(:user) }

  describe 'GET index' do
    before do
      create_list(:user, 3)
      login_user(user)
    end

    it 'should render the index template' do
      get :index

      expect(response).to render_template(:index)
    end

    it 'should assign the correct instance variables' do
      get :index

      expect(assigns(:users)).not_to be_nil
    end
  end

  describe 'GET edit' do
    before { login_user(user) }

    it 'should render the edit template' do
      get :edit, params: {id: user.id}

      expect(response).to render_template(:edit)
    end

    it 'should assign the correct instance variables' do
      get :edit, params: {id: user.id}

      expect(assigns(:user)).to eq(user)
    end
  end

  context 'when an user is trying to edit its password' do
    before { login_user(user) }

    it 'should render the edit template' do
      get :edit_password, params: {id: user.id}

      expect(response).to render_template(:edit_password)
    end

    it 'should assign the correct instance variables' do
      get :edit, params: {id: user.id}

      expect(assigns(:user)).to eq(user)
    end
  end

  context "when an user is trying to edit someone else's password" do
    let!(:another_user) { create(:user) }

    before { login_user(user) }

    it 'should redirect_to to root_path' do
      get :edit_password, params: { id: another_user.id }

      expect(response).to redirect_to(root_path)
    end
  end

  describe 'POST create' do
    before { login_user(user) }

    it 'should create a user' do
      expect {
        post :create, params: { user: {
          email:                 Faker::Internet.email,
          password:              'supersecret',
          password_confirmation: 'supersecret'
        }}
      }.to change { User.count }.by(1)
    end
  end
end
