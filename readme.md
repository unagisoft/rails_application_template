# Unagi Rails Template 

Este template será utilizado para crear nuevas aplicaciones rails. El mismo incluirá las gemas básicas que usan las aplicaciones desarrolladas en Unagi.

### General

Se crea una aplicación Rails 5 con:
- postgresql
- bootstrap (+ inspinia)
- coffeescript
- select2
- initializers de js

## Algunas gemas incluidas

- draper *for Decorators*
- kaminari *for Pagination*
- font-awesome-rails *for Icons*
- rubocop *for Code styling*

## Uso

1. Clonar el template
```
git clone https://gitlab.com/unagisoft/rails_application_template.git
```

2. Correr el comando para crear una nueva aplicación Rails con el template
```
rails new app_name -m ~/rails_application_template/basic/template.rb --database=postgresql
```
**Importante: como todavía el .ruby-version no existe, la versión de Ruby que 
se use para el `bundle install` será la que responda a `rbenv global`.**

3. Configura la zona horaria (`time_zone`) y el idioma de traducción 
(`default_locale`) desde el archivo `application.rb`.

4. Cambiar el archivo `config/database.yml.sample` y renombrarlo.

5. Corre `bundle install`.

6. Eso es todo! A disfrutar :)

#### Frase de la semana
> Dime con quién andas y, si es un perro lazarillo, podremos saber que tienes problemas visuales.

---

*Creado y mantenido por [Unagi](unagi.com.ar)*

