class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  NOT_AUTHORIZED = 'No tenés los permisos necesarios para realizar esta acción.'.freeze

  def user_not_authorized
    respond_to do |format|
      format.html do
        flash[:error] = NOT_AUTHORIZED
        redirect_to root_path
      end
      format.js do
        render json: { msg: NOT_AUTHORIZED }, status: :forbidden
      end
    end
  end
end
