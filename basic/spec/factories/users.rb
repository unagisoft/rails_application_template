FactoryBot.define do
  factory :user do
    email { Faker::Internet.email }
    password 'supersecret'
    password_confirmation 'supersecret'
  end
end
