set :stages, %w(production staging)
set :default_stage, 'staging'

set :repo_url,        'git@gitlab.com:unagisoft/YOUR_AMAZING_APP.git'
set :application,     'YOUR_AMAZING_APP'
set :user,            'deploy'
set :puma_threads,    [4, 16]
set :puma_workers,    0
set :pty,             true
set :use_sudo,        false
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user) }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :pg_system_user, 'postgres'
set :pg_system_db, 'postgres'
set :puma_init_active_record, true  # Change to false when not using ActiveRecord
set :rbenv_ruby, '2.4.0'

set :branch, ENV['BRANCH'] if ENV['BRANCH']
set :keep_releases, 5

## Linked Files & Directories (Default None):
set :linked_files, %w{config/database.yml config/master.key}
set :linked_dirs, %w{log}

namespace :puma do
  desc 'Create Directories for Puma Pids and Socket'
  task :make_dirs do
    on roles(:app) do
      execute "mkdir #{shared_path}/tmp/sockets -p"
      execute "mkdir #{shared_path}/tmp/pids -p"
    end
  end
  before :start, :make_dirs
end

namespace :deploy do
  desc "Make sure local git is in sync with remote."
  task :check_revision do
    on roles(:app) do
      unless `git rev-parse HEAD` == `git rev-parse origin/#{fetch(:branch)}`
        puts "WARNING: HEAD is not the same as origin/#{fetch(:branch)}"
        puts "Run `git push` to sync changes."
        exit
      end
    end
  end

  desc 'Creates database'
  task :create_database do
    on roles(:app), primary: true do |host|
      if database_exists?
        puts 'The database already exists. Skipping creation'
      else
        puts 'Database does not exist. Creating DB'
        rails_env = fetch(:stage)
        execute_interactively "$HOME/.rbenv/bin/rbenv exec bundle exec rails db:create RAILS_ENV=#{rails_env}"
      end

    end
  end

  desc 'Initial Deploy'
  task :initial do
    on roles(:app) do
      execute %[mkdir -p "#{deploy_to}"]

      execute %[mkdir -p "#{shared_path}/config"]
      execute %[chmod g+rx,u+rwx "#{shared_path}/config"]

      execute %[touch "#{shared_path}/config/database.yml"]
      execute %[touch "#{shared_path}/config/secrets.yml"]
      execute %[echo "-----> Be sure to edit '#{shared_path}/config/database.yml' and 'secrets.yml'"]
    end
  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke 'puma:restart'
    end
  end

  before :starting,     :check_revision
  before :starting,     :create_database
  after  :finishing,    :compile_assets
  after  :finishing,    :cleanup
  after  :finishing,    :restart

  def database_exists?
    db_file = capture "cat #{shared_path}/config/database.yml"
    st = YAML.load(db_file)[fetch(:stage).to_s]
    cmd = "psql -U #{fetch(:pg_system_user)} -d #{fetch(:pg_system_db)} -tAc \"SELECT 1 FROM pg_database WHERE datname='#{st['database']}';\" | grep -q 1"
    test *cmd
  end
end

namespace :whenever do
  desc "Loads whenever to cron"
  task :update do
    on roles(:app), primary: true do |host|
      rails_env = fetch(:stage)
      if rails_env == 'production'
        execute_interactively "whenever:update"
      end
    end
  end
end


namespace :rails do
  desc "Open the rails console on each of the remote servers"
  task :console do
    on roles(:app), primary: true do |host|
      rails_env = fetch(:stage)
      execute_interactively "$HOME/.rbenv/bin/rbenv exec bundle exec rails console #{rails_env}"
    end
  end

  task :logs do
    on roles(:app), primary: true do |host|
      rails_env = fetch(:stage)
      execute_interactively "tail -f log/#{rails_env}.log"
    end
  end

  def execute_interactively(command)
    user = fetch(:user)
    port = fetch(:port) || 22
    exec "ssh -l #{user} #{host} -p #{port} -t 'cd #{deploy_to}/current && #{command}'"
  end
end

# ------------------------ DB ------------------------------
#
set :backup_path, "#{shared_path}/db_backups"
namespace :db do
  desc 'Backup production database'
  task :backup do
    on roles(:app) do
      backup_path = fetch(:backup_path)
      execute "mkdir -p #{backup_path}"
      outfile = "#{fetch(:application)}.#{Time.now.to_i}.dump"
      db_file = capture "cat #{shared_path}/config/database.yml"
      st = YAML.load(db_file)[fetch(:stage).to_s]
      execute "pg_dump -Fc --clean postgresql://#{st['username']}:#{st['password']}@#{st['host']}:#{st['port']}/#{st['database']} > #{backup_path}/#{outfile}"
    end
  end

  desc 'Download latest backup'
  task :download do
    on roles(:app) do
      invoke "db:backup"
      backup_path = fetch(:backup_path)
      within backup_path do
        outfile = capture 'find', '.', '-type', 'f', '-name', "#{fetch(:application)}\"*.dump\"", '-printf',
          '"%T@ %p\n"', '|', 'sort', '-n', '|', 'tail', '-1', '|', 'cut', '-f2-', '-d" "'
        outfile = outfile.split('/').last
        download! "#{backup_path}/#{outfile}", outfile
      end
    end
  end

  desc 'Import downloaded Prod DB to Dev'
  task :clone do
    invoke "db:download"
    db_file = `cat config/database.yml`
    st = YAML.load(db_file)['development']
    file = `ls -t #{fetch(:application)}*dump | head -1 | xargs echo -n`
    if st['password'].nil? || st['username'].nil?
      `pg_restore --verbose --clean --no-acl --no-owner -h localhost -d #{st['database']} #{file}`.chomp
    else
      `PGPASSWORD=#{st['password']} pg_restore --verbose --clean --no-acl --no-owner -h 127.0.0.1 -U #{st['username']} -d #{st['database']} #{file}`.chomp
    end
    `rm #{file}`
  end

  desc 'Upload dev DB to Prod'
  task :upload_dev do
    if fetch(:stage) == 'production'
      puts 'Pará pará pará! ¿En serio querés copiar la BD local a ¡¡¡PRODUCCIÓN!!!!?'
      ask :value, "Escribi el nombre de la aplicación para continuar:", nil

      if fetch(:value) != fetch(:application)
        puts "\nOk, no hacemos nada"
        exit
      end
    end

    invoke 'puma:stop'
    backup_path = fetch(:backup_path)
    db_file = `cat config/database.yml`
    st = YAML.load(db_file)['development']
    outfile = "#{fetch(:application)}_development.#{Time.now.to_i}.dump"
    `pg_dump -U #{st['username']} #{st['database']} > #{outfile}`
    `gzip -f #{outfile}`

    on roles(:all) do
      execute "mkdir -p #{backup_path}"
      upload! "#{outfile}.gz", backup_path
      db_file = capture "cat #{shared_path}/config/database.yml"
      st = YAML.load(db_file)[fetch(:stage).to_s]

      execute "zcat #{backup_path}/#{outfile}.gz > #{backup_path}/#{outfile}"
      rails_env = fetch(:stage)
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "db:drop db:create RAILS_ENV=#{rails_env}"
        end
      end
      db_file = capture "cat #{shared_path}/config/database.yml"
      st = YAML.load(db_file)[fetch(:stage).to_s]
      puts "Como diría el gordito de Jurassic Park: 'NO NO NO'. Decime la password de la DB"
      execute "VERBOSITY=verbose psql -U #{st['username']} #{st['database']} -f #{backup_path}/#{outfile}"

      execute "rm #{backup_path}/#{outfile} #{backup_path}/#{outfile}.gz"
    end
  end
end
# ps aux | grep puma    # Get puma pid
# kill -s SIGUSR2 pid   # Restart puma
# kill -s SIGTERM pid   # Stop puma

