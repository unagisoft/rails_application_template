require 'bundler'
#Add the current directoy to the path Thor uses to look up files
#(check Thor notes)

#Thor uses source_paths to look up files that are sent to file-based Thor acitons
#https://github.com/erikhuda/thor/blob/master/lib%2Fthor%2Factions%2Ffile_manipulation.rb
#like copy_file and remove_file.

#We're redefining #source_path so we can add the template dir and copy files from it
#to the application
def source_paths
  Array(super)
  [File.expand_path(File.dirname(__FILE__))]
end


app_name = ask("¿Cómo querés llamar la aplicación?")
app_name = app_name.underscore

# ---------------------------------------
# ---------------------------------------
# Ruby Version
# ---------------------------------------
# copy_file '.ruby-version' # Set local ruby version using rbenv
# ---------------------------------------
# ---------------------------------------


# ---------------------------------------
# ---------------------------------------
# GEMFILE
# ---------------------------------------
remove_file 'Gemfile'
run 'touch Gemfile'
#be sure to add source at the top of the file
add_source 'https://rubygems.org'
gem 'bootsnap', require: false
gem 'bootstrap-sass', '~> 3.3.6'
gem 'bootstrap3-datetimepicker-rails', '~> 4.17.37' # Datetimepicker
gem 'bootstrap4-kaminari-views' # Styles for paginator
gem 'coffee-rails', '~> 4.2'
gem 'draper', '~> 3.0.0.pre1' # Decorators
gem 'font-awesome-rails' # Font awesome icons
gem 'jbuilder', '~> 2.5'
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'kaminari', '~> 0.17.0' # Paginator
gem 'momentjs-rails', '>= 2.9.0' # Datetimepicker dependency
gem 'non-stupid-digest-assets'
gem 'pg', '~> 0.15' # Postgresql
gem 'platform-api' # Heroku
gem 'puma'
gem 'pundit' # Permissions
gem 'rails', '~> 5.2.0'
gem 'rubocop', require: false
gem 'sass-rails', '~> 5.0'
gem 'sorcery', '~> 0.12.0' # Authentication
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'

gem_group :development, :test do
  gem 'factory_bot_rails' # Open email instead of send it
  gem 'pry-byebug' # Debugging
  gem 'rspec-rails'
end

gem_group :development do
  gem 'listen', '~> 3.1', '>= 3.1.5'
  gem 'spring'
  gem 'web-console', '~> 3.0'
end

gem_group :test do
  gem 'faker'
  gem 'rails-controller-testing'
  gem 'shoulda-matchers', '~> 3.1'
  gem 'simplecov', require: false
end

gem_group :production do
  gem 'capistrano',         require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-rbenv',     require: false
  gem 'capistrano3-puma',   require: false
  gem 'rack-cache', require: 'rack/cache'
end

run 'bundle install'
# ---------------------------------------



# ---------------------------------------
# VENDOR
# ---------------------------------------
remove_dir 'vendor/assets'
inside 'vendor' do
  directory 'assets' # Copy the entire assets folder
  inside 'assets' do
    inside 'javascripts' do
      run "ln -s ../plugins/ plugins" # Build symbolic links
    end
    inside 'stylesheets' do
      run "ln -s ../plugins/ plugins" # Build symbolic links.
    end
  end
end
# ---------------------------------------



# ---------------------------------------
# SCAFFOLD
# ---------------------------------------
inside 'lib' do
  directory 'templates' # Copy the entire assets folder
end
# ---------------------------------------



# ---------------------------------------
# APP-ASSETS
# ---------------------------------------
remove_dir 'app/assets'
inside 'app' do
  directory 'assets' # Copy the entire assets folder
end
# ---------------------------------------



# ---------------------------------------
# PUBLIC
# ---------------------------------------
remove_dir 'public'
directory 'public' # Copy the entire public folder
# ---------------------------------------



# ---------------------------------------
# APP
# ---------------------------------------
run "mkdir app/decorators"
run "mkdir app/filters"
run "mkdir app/presenters"
run "mkdir app/services"
remove_dir 'app/channels'
remove_dir 'app/helpers'
remove_dir 'app/jobs'
remove_dir 'app/mailers'

inside 'app' do
  directory 'helpers' # Copy the entire helpers folder
  directory 'mailers'
end
# ---------------------------------------




# ---------------------------------------
# Generate Routes
# ---------------------------------------
remove_file 'config/routes.rb'
inside 'config' do
  copy_file 'routes.rb'
end
# ---------------------------------------




# ---------------------------------------
# APPLICATION_CONTROLLER

inject_into_file 'app/controllers/application_controller.rb', after: /ActionController::Base\n/ do
  <<-RUBY
  protect_from_forgery with: :exception
  RUBY
end

generate 'pundit:install'
inject_into_file 'app/controllers/application_controller.rb', after: /exception\n/ do
  <<-RUBY
  include Pundit
  rescue_from Pundit::NotAuthorizedError, with: :user_not_authorized

  NOT_AUTHORIZED = 'No tenés los permisos necesarios para realizar esta acción.'.freeze

  def user_not_authorized
    respond_to do |format|
      format.html do
        flash[:error] = NOT_AUTHORIZED
        redirect_to root_path
      end
      format.js do
        render json: { msg: NOT_AUTHORIZED }, status: :forbidden
      end
    end
  end
  RUBY
end
# ---------------------------------------




# ---------------------------------------
# VIEWS / LAYOUT
# ---------------------------------------
remove_dir 'app/views'
inside 'app' do
  inside 'views' do
    directory 'application'
    directory 'layouts'
    directory 'main'
  end
end
# ---------------------------------------




# ---------------------------------------
# INITIALIZERS
# ---------------------------------------
remove_dir 'config/initializers'
inside 'config' do
  directory 'initializers' # Copy the initializers folder
  directory 'locales' # Copy the locales folder
end
# ---------------------------------------




# ---------------------------------------
# TESTS
# ---------------------------------------
remove_dir 'test'
inside 'spec' do
  copy_file 'rails_helper.rb'
  copy_file 'spec_helper.rb'
end
# ---------------------------------------




# ---------------------------------------
# Sorcery
# ---------------------------------------
if yes?("Deseas incluir Sorcery? Creará un login sin roles de usuario. [Yes/No]")
  generate 'sorcery:install'

  inside 'app/controllers' do
    copy_file 'sessions_controller.rb'
    copy_file 'users_controller.rb'
  end

  inject_into_file 'app/controllers/application_controller.rb', after: /exception\n/ do
  <<-RUBY
  before_action :require_login
  RUBY
  end

  inject_into_file 'app/controllers/application_controller.rb', after: /\.freeze\n/  do
  <<-RUBY
  NOT_AUTHENTICATED = 'Debes iniciar sesión para acceder a esta página.'.freeze

  def not_authenticated
    flash[:error] = NOT_AUTHENTICATED
    redirect_to login_path
  end
  RUBY
  end

  inside 'app/views/application' do
    insert_into_file '_aside.html.erb', after: /\'briefcase\' \%\>\n/ do
    <<-RUBY
    <% if policy(User).index? %>
      <%= side_link text: 'Usuarios',
          path:       users_path,
          icon_class: 'users'
      %>
    <% end %>
    RUBY
    end
  end

  route "resources :users, only: [:index, :new, :create, :edit, :update, :destroy] do
      member do
        get :edit_password
        patch :change_password
      end
    end\n"

  route "get 'login', to: 'sessions#new', as: :login"
  route "delete 'logout', to: 'sessions#destroy', as: :logout"
  route "resources :sessions, only: [:create]"


  inside 'app/views/application' do
    insert_into_file '_navbar.html.erb', before: /<\/nav>/ do
    <<-RUBY
    <% if logged_in? %>
      <ul class="nav navbar-top-links navbar-right">
        <li class="dropdown">
          <a data-toggle="dropdown" class="dropdown-toggle" href="#">
            <%= current_user.email %>
            </span><b class="caret"></b>
            </span>
          </a>
          <ul class="dropdown-menu animated fadeInRight m-t-xs">
            <li>
              <%= link_to 'Editar perfil', [:edit, current_user] %>
            </li>
            <li>
              <%= link_to 'Cambiar contraseña', [:edit_password, current_user] %>
            </li>
            <li class="divider"></li>
            <li>
              <%= link_to 'Cerrar sesión', logout_path, method: :delete %>
            </li>
          </ul>
        </li>
      </ul>
    <% end %>
    RUBY
    end
  end

  remove_dir 'app/views/sessions'
  remove_dir 'app/views/users'
  remove_file 'app/models/user.rb'
  empty_directory 'app/policies'
  inside 'app' do
    inside 'views' do
      directory 'sessions'
      directory 'users'
    end
    inside 'models' do
      copy_file 'user.rb'
    end
    inside 'policies' do
      copy_file 'user_policy.rb'
    end
  end

  inside 'spec' do
    directory 'controllers'
    directory 'models'
    directory 'factories'

    inside 'controllers' do
      copy_file 'users_controller_spec.rb'
    end

    inside 'models' do
      copy_file 'user_spec.rb'
    end

    inside 'factories' do
      copy_file 'users.rb'
    end
  end
end
# ---------------------------------------



# ---------------------------------------
# Database
# ---------------------------------------
remove_file 'config/database.yml'
file 'config/database.yml.sample', <<-RUBY
default: &default
  adapter: postgresql
  encoding: unicode
  pool: <%= ENV.fetch("RAILS_MAX_THREADS") { 5 } %>
  #username:
  #password:

development:
  <<: *default
  database: #{app_name}_development

test:
  <<: *default
  database: #{app_name}_test
RUBY
# ---------------------------------------



# ---------------------------------------
# CI
# ---------------------------------------
remove_file 'config/database.yml.gitlab'
inside 'config' do
  copy_file 'database.yml.gitlab'
end
copy_file '.gitlab-ci.yml'
# ---------------------------------------



# ---------------------------------------
# Application Configuration
# ---------------------------------------
inside 'config' do
  insert_into_file 'application.rb', :after => /class Application \< Rails\:\:Application\n/ do
    <<-RUBY
    config.generators do |g|
      g.helper false
      g.stylesheets false
      g.javascripts false
      g.view_specs false
      g.jbuilder false
    end
    RUBY
  end
end
# ---------------------------------------



# ---------------------------------------
# Readme and setup
# ---------------------------------------
remove_file 'bin/setup'
inside 'bin' do
  copy_file 'setup'
end
run 'chmod +x bin/setup'
# ---------------------------------------



# ---------------------------------------
# Changelog
# ---------------------------------------
run 'touch CHANGELOG'
# ---------------------------------------



# ---------------------------------------
# Capistrano
# ---------------------------------------
copy_file 'Capfile'
inside 'config' do
  copy_file 'deploy.rb'
  directory 'deploy'
end

inside 'config' do
  inside 'environments' do
    copy_file 'staging.rb'
  end
end

# ---------------------------------------



# ---------------------------------------
# Generate MainController
# ---------------------------------------
inside 'app/controllers' do
  copy_file 'main_controller.rb'
end
remove_dir 'app/views/main'
inside 'app' do
  inside 'views' do
    directory 'main'
  end
end
# ---------------------------------------



# ---------------------------------------
# Rubocop
# ---------------------------------------
  run 'touch .rubocop_todo.yml'
  copy_file '.rubocop.yml'
# ---------------------------------------



# ---------------------------------------
# Git, Readme, Rubocop
# ---------------------------------------
after_bundle do
  remove_file 'README.rdoc'
  git :init
  git checkout: "-b develop"
  git add: '.'
  git commit: "-a -m 'Initial commit'"
end
# ---------------------------------------
