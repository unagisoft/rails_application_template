class UsersPresenter

  def initialize(params)
    @params = params
  end

  def users
    @users ||= User.all.page(@params[:page]).per(20)
  end

  def title
    'Usuarios'
  end

end

