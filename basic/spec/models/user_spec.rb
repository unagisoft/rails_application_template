require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'Basics' do
    it 'has a valid factory' do
      expect(build(:user)).to be_valid
    end
  end

  describe 'Validations' do
    context 'presence validations' do
      it { should validate_presence_of(:email) }
      it { should validate_presence_of(:password) }
      it { should validate_presence_of(:password_confirmation) }
    end

    context 'uniqueness validations' do
      subject { create(:user) }

      it { should validate_uniqueness_of(:email) }
    end

    context 'length validations' do
      it { should validate_length_of(:password).is_at_least(8) }
    end
  end
end
